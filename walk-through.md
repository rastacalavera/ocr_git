### Getting started with Git

This guide will use the following skills/Resources:
* command line interface (CLI)
* package installation
* GitLab account
* web browsing

The author of this guide is using Ubuntu 20.10, if  you are using a non-ubuntu based distro, some commands will not work for you.


The difficulty rating for this task is a 4 out of 10 assuming you have a basic proficiency in the skill set. If not, it could be closer to 7 out of 10.

## From Episode 201, "git init"

#### Step 1. Install and configure git
Open a terminal using a GUI or shortcuts (ctrl+alt+t on most distros)
and install the git package.

`sudo apt install git`

Now, configure the idenity for git that will be used publicly. 

KEEP the quotation marks ("") and replace the text inside of them with your desired user name.

`git config --global user.name "John Doe"`

Configure your email

`$ git config --global user.email johndoe@example.com`

#### Step 2. Generate ssh keys
If you already have ssh keys, this can be skipped.

In your terminal, run the following command to generate your keys:

`ssh-keygen -t rsa`

Now verify that the .ssh folder exists and that you can locate your keys using the following commands:

`cat ~/.ssh/id_rsa.pub`

If a long string of text is the resulting output, then you are good to go!

#### Step 3. GitLab and ssh Keys
After creating your GitLab account, add your PUBLIC ssh key to the account by going into settings --> ssh keys (on the left hand side of the screen)

#### Step 4. Creating a git Repo
In your terminal, make a folder that will serve as your repository. For this example, I have a "git" folder in my home directory that stores all of my repos. My working directory is `/home/rastacalavera/git` and the repo that will be created will be called "cli". 
* Create folder `mkdir cli`
* Enter folder `cd cli`
* Initialize git `git init`
* Create a file `nano DLN.md`
    * in the file, just type something fun, then exit using ctrl+x and save your changes.
* Add the file to git `git add DLN.md`
* Create a commit record for the file `git commit -m "my first addition"`
* Push your private repository to GitLab `git push --set-upstream git@gitlab.com:rastacalavera/cli.git master`
    * replace my username with your GitLab username. If you are successful, you should see something like this:
```Enumerating objects: 3, done.
Counting objects: 100% (3/3), done.
Writing objects: 100% (3/3), 237 bytes | 237.00 KiB/s, done.
Total 3 (delta 0), reused 0 (delta 0), pack-reused 0
remote: 
remote: 
remote: The private project rastacalavera/cli was successfully created.
remote: 
remote: To configure the remote, run:
remote:   git remote add origin git@gitlab.com:rastacalavera/cli.git
remote: 
remote: To view the project, visit:
remote:   https://gitlab.com/rastacalavera/cli
remote: 
remote: 
remote: 
To gitlab.com:rastacalavera/cli.git
 * [new branch]      master -> master
Branch 'master' set up to track remote branch 'master' from 'git@gitlab.com:rastacalavera/cli.git'.    
```
Congrats! You now have a git repo you can push and pull from! If you create a new file in your folder, follow these steps to upload it:
* put your file name after add 
    * `git add` 
* type in some information about the file 
    * `git commit -m "REPLACE ME"` 
* this sends it to your repo
    * `git push` 

If you make changes to an existing file, just skip the "git add" portion of the above commands.

## From Episode 202, "git clone"

Unlink "git init" which initializes a local folder on your machine, "git clone" copies a repository from it's "master" branch which is hosted on GitLab and places it on your machine which is referred to as the "origin".

We are going to add an extra step in here called a "fork". A fork copies/clones a GitLab project into your personal account. Any changes you make stay inside of your copy and is not sent back to the original project. Once you have forked a project, we will use the "git clone" option to get a local copy on the local machine.

#### Step 1. Fork Sample Project.
* First, you need to fork this [sample project](https://gitlab.com/gitlab-tests/sample-project/). If you don't see the fork icon, look in the README document where they have an image to reference.

#### Step 2. Clone the Repository 
* Once the webpage refreshes, you can click on the blue clone button and copy the "Clone with ssh" option.
* In your terminal, make sure you are in the location where you want this folder to live. You do not need to make a folder for this project, the clone command makes that for you.
    For example, this is my working directory for git projects:

    `/home/rastacalavera/git`

    The folders that live in this directory are:

    `backups  cli  my-homelab  ocr_git`

    and once I clone this project, a new folder will be created here called "Sample-Project".
* Use the command "git clone" and then paste the text that you copied from the GitLab fork.
    ` git clone git@gitlab.com:rastacalavera/sample-project.git`    
    You should see a result like this:
```Cloning into 'sample-project'...
remote: Enumerating objects: 12, done.
remote: Counting objects: 100% (12/12), done.
remote: Compressing objects: 100% (10/10), done.
remote: Total 12 (delta 1), reused 0 (delta 0), pack-reused 0
Receiving objects: 100% (12/12), 30.73 KiB | 899.00 KiB/s, done.
Resolving deltas: 100% (1/1), done.
```
    
Your working directory should now include the new cloned project

`backups  cli  my-homelab  ocr_git  sample-project`

## Episode 203, "git add"

Well, this tutorial already mentioned how to use "git add" and "git commit" in the first section so . . . . why not use these commands in the repo that you cloned previously? Here is a challenge:
* create a file plain text file
* include what you think you may use git for AND some cool ASCII art with the source cited.
* push your new file to the repo and in the commit write something silly to a DLN host
* link your repo in the comments

## Episode 204, "git config --global" email and username

This was covered in the "git init" section. It is important to note that this command will apply to every directory that is being tracked by git.
 To display the global configuration, the following commands can be used:

`git config --global --list`

You should see the user name that you set previously as the returned value.

You can also display individual information by using `git config --global user.name` or `git config --global user.email`.

If you want to use a different username and email for a specific repository, that is possible as well!!

MORE ON THIS SOON!

#### Summary and Resources
`git init`  (Episode [201](https://youtu.be/jfNyKIa23m0?t=3386), time 00:56:26)

`git clone` (Episode [202](https://youtu.be/Px-nQxr1iVU?t=2098), time 00:34:57)

`git add` (Episode [203](), time XX:XX:XX)

`git config --global` (Episode [204](), time XX:XX:XX)

GitLab [Push New Project](https://docs.gitlab.com/ee/gitlab-basics/create-project.html#push-to-create-a-new-project)
GitLab [Printable Cheatsheet](https://about.gitlab.com/images/press/git-cheat-sheet.pdf)
