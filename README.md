# OCR_Git- 
Community Companion for DLN shorts on Git
### I am not affiliated with DLN or their crew. I am just a listener and community member.

An [old discourse thread](https://discourse.destinationlinux.network/t/getting-started-in-home-networking/1051/25) of mine got a comment recently and kind of rekindled my interest in this project.

I figured since DLN is doing shorts on the Linux file system and git, this would be a good companion for community members to dive in deeper with.