# Resources
* [DLN Discourse Forum Post](https://discourse.destinationlinux.network/t/getting-started-in-home-networking/1051/25)
* [GitLab Command Line Documentation](https://docs.gitlab.com/ee/gitlab-basics/README.html)
* [ORC_Git Repo](https://gitlab.com/rastacalavera/ocr_git)
* [ssh information](https://www.ssh.com/ssh/keygen/)